package cn.wolfcode.javabase;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;

/**
 * @ClassName LocalDateTimeDemo
 * @Description 时间类（java8类）
 * @Author xdzhango
 * @Date 2020/4/22  11:15
 **/
public class LocalDateTimeDemo {
    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("当前时间"+localDateTime);
        System.out.println("当前年份"+localDateTime.getYear());
        System.out.println("当前月份"+localDateTime.getMonth());
        System.out.println("当前日"+localDateTime.getDayOfMonth());
        System.out.println("当前时"+localDateTime.getHour());
        System.out.println("当前分"+localDateTime.getMinute());
        System.out.println("当前秒"+localDateTime.getSecond());


        //指定的格式
        LocalDateTime beforDate = LocalDateTime.of(2020,Month.APRIL,22,11,57,59);
        System.out.println(beforDate);

        //格式化日期
        String format = beforDate.format(DateTimeFormatter.ISO_DATE);
        String format1 = beforDate.format(DateTimeFormatter.ISO_DATE_TIME);
        String format2 = beforDate.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        System.out.println(format);
        System.out.println(format1);
        System.out.println(format2);
    }
}
