package cn.wolfcode.javabase;

/**
 * @Author: Wjj
 * @Date: 2020/4/22 12:37 上午
 * @desc: 数组是值传递而非引用传递
 */
public class _01数组是值传递 {
    public static void main(String[] args) {
        Integer[] array = {2, 3, 4};
        changeArray(array);
        System.out.println(array); // 还是2，3，4
    }

    static void changeArray(Integer[] array) {
        Integer[] newArray = {1, 3, 5};
        array = newArray;
    }
}
