package cn.wolfcode.thread;

import java.util.concurrent.atomic.AtomicInteger;

public class TestIncrement {
    public static void main(String[] args) {
        IncrementDemo incrementDemo = new IncrementDemo();
        for (int i = 0; i <10 ; i++) {
             new Thread(incrementDemo).start();
        }
    }

}

class IncrementDemo implements Runnable{
//    private int num = 0;
    private AtomicInteger num = new AtomicInteger(0);//解决并发安全问题
    public void run() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(getNum());
    }

    public int getNum() {
//        return num++;
        return num.getAndIncrement();
    }

}