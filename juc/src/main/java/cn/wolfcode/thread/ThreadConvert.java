package cn.wolfcode.thread;

/**
 * 面试题
 * 现在有T1、T2、T3三个线程，
 * 你怎样保证T2在T1执行完后执行，T3在T2执行完后执行？
 */
public class ThreadConvert {
    public static void main(String[] args) throws InterruptedException {
        ConverDemo converDemo = new ConverDemo();
        /*Thread t1 = new Thread(converDemo,"T1");
        Thread t2 = new Thread(converDemo,"T2");
        Thread t3 = new Thread(converDemo,"T3");

        t1.start();//必须先启动,后join
        t1.join();
        t2.start();//必须先启动,后join
        t2.join();
        t3.start();//必须先启动,后join*/
        method2();


    }

    private static void method2() {
        Thread t1 = new Thread(new Runnable() {
            public void run() {
                System.out.println("T1");
            }
        });
        Thread t2 = new Thread(new Runnable() {
            public void run() {
                try {
                    t1.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("T2");
            }
        });
        Thread t3 = new Thread(new Runnable() {
            public void run() {
                try {
                    t2.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("T3");
            }
        });
        t3.start();
        t2.start();
        t1.start();
    }

}

class ConverDemo implements Runnable{

    public void run() {
        for (int i = 0; i <20 ; i++) {
            System.out.println(Thread.currentThread().getName());
        }
    }
}

