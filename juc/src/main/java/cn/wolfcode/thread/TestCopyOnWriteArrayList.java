package cn.wolfcode.thread;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * concurrent.CopyOnWriteArrayList
 */
public class TestCopyOnWriteArrayList {
    public static void main(String[] args) {
        CopyOnWriteDemo copyOnWriteDemo = new CopyOnWriteDemo();
        for (int i = 0; i <2 ; i++) {
            new Thread(copyOnWriteDemo).start();
          /*  try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/

        }

    }
}

class CopyOnWriteDemo implements Runnable{
//    private static ArrayList arrayList = new ArrayList();
    private  static CopyOnWriteArrayList arrayList = new CopyOnWriteArrayList();
    static {
        arrayList.add("AA");
        arrayList.add("BB");
        arrayList.add("CC");
    }

    public void run() {
        Iterator iterator = arrayList.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());

            arrayList.add("DD");
        }
        System.out.println(arrayList);
    }
}
