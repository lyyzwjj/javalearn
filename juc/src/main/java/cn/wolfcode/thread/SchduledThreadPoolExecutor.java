package cn.wolfcode.thread;

import java.util.concurrent.*;

/**
 * 延迟1秒
 */
public class SchduledThreadPoolExecutor {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //延迟1秒
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(5);
        for (int i = 0; i <5 ; i++) {
            ScheduledFuture<Integer> schedule = pool.schedule(new Callable<Integer>() {
                public Integer call() throws Exception {
                    int sum = 0;
                    for (int i = 0; i < 5; i++) {
                        sum += i;
                    }
                    return sum;
                }
            }, 1, TimeUnit.SECONDS);
            System.out.println(schedule.get());

        }
        //关闭
        pool.shutdown();
    }
}
