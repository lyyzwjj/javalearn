package cn.wolfcode.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class CreateThreadTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //1
//        RunnableDemo runnableDemo = new RunnableDemo();
//        new Thread(runnableDemo).start();
        //2
//        new ThreadDemo().start();
        //3
        CallableDemo callableDemo = new CallableDemo();
        FutureTask<Integer> futureTask = new FutureTask<Integer>(callableDemo);
        new Thread(futureTask).start();

        Integer res = futureTask.get();
        System.out.println(res);
    }
}
//方式一实现Runnable
class RunnableDemo implements Runnable {

    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }
    }
}

//方式二继承Thread
class ThreadDemo extends Thread{
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }
    }
}

//方式三实现Callable接口
class CallableDemo implements Callable<Integer>{

    public Integer call() throws Exception {
        int sum = 0;
        for (int i = 0; i <5 ; i++) {
            sum += i;
        }
        return sum;
    }
}