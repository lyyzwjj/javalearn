package cn;

import org.redisson.Redisson;
import org.redisson.config.Config;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @ClassName Application
 * @Description TOOD
 * @Author xdzhango
 * @Date 2020/4/24 0024 15:20
 **/
@SpringBootApplication
public class RedisDeomApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisDeomApplication.class, args);
    }

    @Bean
    public Redisson redisson(){
        //此为单机模式
        Config config = new Config();
        config.useSingleServer().setAddress("redis://wjjzst.com:6379").setDatabase(0);
        return (Redisson) Redisson.create(config);
    }
}
