package cn.wolfcode.repository;

import cn.wolfcode.domain.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * 泛型1:操作类型
 * 泛型2:id类型
 */
public interface UserEsRepository extends ElasticsearchRepository<User, Long> {
    //自定义高级查询方法
}
