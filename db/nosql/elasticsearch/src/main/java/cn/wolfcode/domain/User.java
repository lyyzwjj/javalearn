package cn.wolfcode.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@ToString
@Getter
@Setter
//@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "user",type = "user")
public class User {

    @Id
    private Long id;
    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String name;
    private Integer age;
    private Long deptId;

    public User(Long id, String name, Integer age, Long deptId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.deptId = deptId;
    }
}
