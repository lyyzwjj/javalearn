package cn.wolfcode;

import cn.wolfcode.domain.User;
import cn.wolfcode.repository.UserEsRepository;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.LongTerms;
import org.elasticsearch.search.aggregations.metrics.stats.InternalStats;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

/**
 * 需先按照elasticsearch等服务
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ElasticsearchApplicationTests {

	@Autowired
	private UserEsRepository repository;
	@Autowired
	private ElasticsearchTemplate template;

	@Test
	public void  test() throws Exception{
		User u = new User(2L,"逍遥2",19,2L);
		repository.save(u);
//		System.out.println("111");
	}

	@Test
	public void  get() throws Exception{
		Optional<User> user = repository.findById(2L);
		user.ifPresent(System.out::print);
	}

	@Test
	public void  list() throws Exception{
		Iterable<User> all = repository.findAll();
		for (User user : all) {
			System.out.print(user);

		}

	}

	@Test
	public void  findName() throws Exception{
//		repository.find();

	}
	@Test
	public void  delete() throws Exception{
		repository.deleteById(2L);

	}

	//高级查询
	//分页查询文档,显示第二页,每页显示3个,按照age升序排序
	@Test
	public void  queryForPage() throws Exception{
		NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
		builder.withPageable(PageRequest.of(1,3));
		builder.withSort(SortBuilders.fieldSort("age").order(SortOrder.ASC));

		AggregatedPage<User> users = template.queryForPage(builder.build(), User.class);
		users.forEach(System.out::print);

	}

	//查询所有name含有zhang的文档
	@Test
	public void  queryName() throws Exception{
		NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
		builder.withQuery(QueryBuilders.matchQuery("name","的"));
		List<User> users = template.queryForList(builder.build(), User.class);
		users.forEach(System.out::print);

	}

	//查询所有name含有zhang的文档 或者age  =5
	@Test
	public void  queryNameOr() throws Exception{
		NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
		builder.withQuery(
				QueryBuilders.boolQuery()
				.should(QueryBuilders.matchQuery("name","的"))
				.should(QueryBuilders.matchQuery("age",5))
		);
		List<User> users = template.queryForList(builder.build(), User.class);
		users.forEach(System.out::print);

	}

	//查询所有name含有zhang的文档 并且3<=age<=7
	@Test
	public void  queryRange() throws Exception{
		NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
		builder.withQuery(
				QueryBuilders.boolQuery()
						.must(QueryBuilders.matchQuery("name","的"))
				        .filter(QueryBuilders.rangeQuery("age").gte(3).lte(7))

		);
		List<User> users = template.queryForList(builder.build(), User.class);
		users.forEach(System.out::print);

	}


	/**
	 * 分组查询
	 * @throws Exception
	GET /user/user/_search
	{
	"size": 0,
	"aggs": {
	"groupByDeptId": {
	"terms": {"field": "deptId" ,"order": {"_term":"asc"}},
	"aggs": {
	"statAge": {
	"stats": {
	"field": "age"
	}
	}
	}
	}
	}
	}
	 *
	 */

	@Test
	public void  queryAggs() throws Exception{
		NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
		builder.withIndices("user").withTypes("user");
		builder.addAggregation(
				AggregationBuilders.terms("groupByDeptId").field("deptId")
		);
		Aggregations aggs = template.query(builder.build(), res -> res.getAggregations());
		LongTerms groupByDeptId = aggs.get("groupByDeptId");
		System.out.println(groupByDeptId.getClass());
		//aggs.forEach(System.out::print);
		List<LongTerms.Bucket> buckets = groupByDeptId.getBuckets();
		for (LongTerms.Bucket bucket : buckets) {
			System.out.println(bucket.getKey());
		}

	}
	@Test
	public void  querySubAggs() throws Exception{
		NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
		builder.withIndices("user").withTypes("user");
		builder.addAggregation(
				AggregationBuilders.terms("groupByDeptId").field("deptId")
				.subAggregation(AggregationBuilders.stats("avgStats").field("age"))
		);
		Aggregations aggs = template.query(builder.build(), res -> res.getAggregations());
		LongTerms groupByDeptId = aggs.get("groupByDeptId");
		System.out.println(groupByDeptId.getClass());
		//aggs.forEach(System.out::print);
		List<LongTerms.Bucket> buckets = groupByDeptId.getBuckets();
		for (LongTerms.Bucket bucket : buckets) {
			InternalStats avgStats = bucket.getAggregations().get("avgStats");
			System.out.println(avgStats.getAvg());
			System.out.println(avgStats.getMaxAsString());
		}

	}

}
