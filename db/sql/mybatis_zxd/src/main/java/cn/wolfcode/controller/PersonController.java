package cn.wolfcode.controller;

import cn.wolfcode.entity.Person;
import cn.wolfcode.service.PersonService;
import cn.wolfcode.service.impl.PersonServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName PersonController
 * @Description TOOD
 * @Author xdzhango
 * @Date 2020/4/21 0021 14:31
 **/
@Api(description = "测试demo")
@Controller("user")
@RequestMapping("/test")
public class PersonController {

    @Autowired
    private PersonService personService;

    @ApiOperation(value = "获取person", notes="根据主键ID获取person数据")
    @ApiImplicitParam(name = "id", value = "主键id", paramType = "query", required = true, dataType = "Long")
    @RequestMapping(value = "getPerson", method=RequestMethod.POST)
    @ResponseBody
    public Person  getPerson(Long id){
        Person person = personService.selectByPrimaryKey(id);
        return person;
    }



}
