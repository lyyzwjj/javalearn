package cn.wolfcode.service.impl;

import cn.wolfcode.entity.Person;
import cn.wolfcode.mapper.PersonMapper;
import cn.wolfcode.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName PersonServiceImpl
 * @Description TOOD
 * @Author xdzhango
 * @Date 2020/4/21 0021 14:30
 **/
@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    private PersonMapper personMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return 0;
    }

    @Override
    public int insert(Person record) {
        return 0;
    }

    @Override
    public int insertSelective(Person record) {
        return 0;
    }

    @Override
    public Person selectByPrimaryKey(Long id) {
        return personMapper.selectByPrimaryKey(1L);
    }

    @Override
    public int updateByPrimaryKeySelective(Person record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(Person record) {
        return 0;
    }
}
