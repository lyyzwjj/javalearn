package cn.wolfcode.db.sql.shardingjdbc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.wolfcode.db.sql.shardingjdbc.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @Author: Wjj
 * @Date: 2020/9/17 1:15 上午
 * @desc:
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}
