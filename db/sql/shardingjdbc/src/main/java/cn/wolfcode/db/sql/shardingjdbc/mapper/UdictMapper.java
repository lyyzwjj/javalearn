package cn.wolfcode.db.sql.shardingjdbc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.wolfcode.db.sql.shardingjdbc.entity.Udict;
import org.springframework.stereotype.Repository;

/**
 * @Author: Wjj
 * @Date: 2020/9/17 1:16 上午
 * @desc:
 */
@Repository
public interface UdictMapper extends BaseMapper<Udict> {
}

