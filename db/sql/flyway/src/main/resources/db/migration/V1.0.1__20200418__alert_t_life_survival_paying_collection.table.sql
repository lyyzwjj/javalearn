ALTER TABLE `flyway`.`t_life_survival_paying_collection` 
DROP COLUMN `biz_channel`,
DROP COLUMN `bank_code`,
DROP COLUMN `bank_account`,
MODIFY COLUMN `create_time` datetime(0) NOT NULL COMMENT '创建时间' AFTER `policy_code`;