package cn.wolfcode.queue.rabbitmq.rabbitmqspringboot.queueConfig;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author: wangzhe
 * @create: 2020/9/23 11:45 上午
 * @Description NormalQueueEnum中的所有普通消息队列批量注入容器
 */
@Component
@Slf4j
public class NormalQueueRegistry implements BeanDefinitionRegistryPostProcessor, ApplicationContextAware {
    private ApplicationContext applicationContext;

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        try {
            for (NormalQueueEnum value : NormalQueueEnum.values()) {
                String queueName = value.getName();
                String queueExchange = value.getExchange();
                String queueKey = value.getRouteKey();
                registry.registerBeanDefinition(queueExchange, BeanDefinitionBuilder.genericBeanDefinition(DirectExchange.class, () -> ExchangeBuilder
                        .directExchange(queueExchange)
                        .durable(true)
                        .build()).getBeanDefinition());
                registry.registerBeanDefinition(queueName, BeanDefinitionBuilder.genericBeanDefinition(Queue.class, () -> new Queue(queueName)).getBeanDefinition());
                registry.registerBeanDefinition(queueKey, BeanDefinitionBuilder.genericBeanDefinition(Binding.class, () -> BindingBuilder
                        .bind(applicationContext.getBean(queueName, Queue.class))
                        .to(applicationContext.getBean(queueExchange, DirectExchange.class))
                        .with(queueKey)).getBeanDefinition());
            }
        } catch (Exception e) {
            log.debug("初始化 NormalQueueEnum 普通队列 Beans 异常");
            e.printStackTrace();
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
