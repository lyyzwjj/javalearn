package cn.wolfcode.queue.rabbitmq.rabbitmqspringboot;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"cn.wolfcode.queue.rabbitmq.rabbitmqspringboot.consumer"})
public class MainConfig {
}
