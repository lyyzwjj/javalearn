package cn.wolfcode.common.exception;

import lombok.Data;

/**
 * @Author: Wjj
 * @Date: 2020/4/18 8:28 下午
 * @desc: 异常DTO
 */

@Data
public class ErrorStatus {
    private String code; // 系统错误码

    private String msg;  // 系统错误信息

    public ErrorStatus(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
