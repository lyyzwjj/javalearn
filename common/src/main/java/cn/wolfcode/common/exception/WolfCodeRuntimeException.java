package cn.wolfcode.common.exception;

/**
 * @Author: Wjj
 * @Date: 2020/4/18 8:15 下午
 * @desc: 本项目公共运行时异常
 */
public class WolfCodeRuntimeException extends RuntimeException {
    public static final String DEFAULT_CODE = "system.error";
    public static final String DEFAULT_DESC = "system.desc";
    private String code = DEFAULT_CODE;
    private String desc = DEFAULT_DESC;

    public WolfCodeRuntimeException() {
        this(DEFAULT_CODE);
    }

    public WolfCodeRuntimeException(String code) {
        super();
        this.code = code;
    }

    public WolfCodeRuntimeException(String code, String desc) {
        super(desc);
        this.code = code;
        this.desc = desc;
    }
    public WolfCodeRuntimeException(ErrorStatus errorStatus) {
        super(errorStatus.getMsg());
        this.code = code;
        this.desc = desc;
    }

}
