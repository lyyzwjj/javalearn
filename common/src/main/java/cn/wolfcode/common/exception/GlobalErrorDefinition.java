package cn.wolfcode.common.exception;

/**
 * @Author: Wjj
 * @Date: 2020/4/18 8:26 下午
 * @desc: 全局异常枚举类
 */
public class GlobalErrorDefinition {
    public static final String UNKNOWN_CODE = "9999";
    public static final ErrorStatus UNKNOWN_ERROR = new ErrorStatus(UNKNOWN_CODE, "系统繁忙，请稍后再试");
}
