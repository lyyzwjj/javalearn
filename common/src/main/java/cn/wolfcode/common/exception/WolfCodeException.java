package cn.wolfcode.common.exception;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @Author: Wjj
 * @Date: 2020/4/18 8:43 下午
 * @desc:
 */
public class WolfCodeException extends  Exception{
    public WolfCodeException() {
        super();
    }

    public WolfCodeException(String message) {
        super(message);
    }

    public WolfCodeException(String message, Throwable e) {
        super(message, e);
        try {
            Files.newDirectoryStream(Paths.get(""));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
