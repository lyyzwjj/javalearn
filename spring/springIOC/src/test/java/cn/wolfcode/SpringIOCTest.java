package cn.wolfcode;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * debug模式
 * springIOC学习夭折
 */
@SpringBootTest
public class SpringIOCTest {


	@Test
	public void  test() throws Exception{
//		System.out.println("111");
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

		Person person = (Person) context.getBean("personTest");
		System.out.println("person"+person);

	}


}
