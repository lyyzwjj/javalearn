package cn.wolfcode;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

//@ConfigurationProperties(prefix = "user") //高版本idea2019
@Configuration
@ComponentScan(basePackages = {"cn.wolfcode"})
public class Config {

    @Bean(name = "personTest")
    public Person person() {
        Person person = new Person();
        person.setUsername("张三");
        person.setAge(18);
        return person;
    }

}
