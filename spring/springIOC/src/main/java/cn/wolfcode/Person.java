package cn.wolfcode;

import java.io.Serializable;

/**
 * @author lengleng
 * @date 2019-11-08
 * <p>
 * 2.2 之前版本,必须使用 @Component 或者 @Configuration 声明成Spring Bean
 */
//@Component
//@ComponentScan()
public class Person implements Serializable{
    private String username;

    private Integer age;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}