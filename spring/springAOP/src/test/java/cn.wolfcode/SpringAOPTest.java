package cn.wolfcode;

import cn.wolfcode.jdk.JdkTest;
import cn.wolfcode.jdk.JdkTestDemo;
import cn.wolfcode.jdk.JdkTestImpl;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @ClassName SpringAOPTest
 * @Description TODO
 * @Author xdzhango
 * @Date 2020/5/19 15:42
 **/
@SpringBootTest
public class SpringAOPTest {

    @Test
    public void Test(){
        JdkTest jdkTest = new JdkTestImpl();
        JdkTestDemo jdkTestDemo = new JdkTestDemo(jdkTest);
        JdkTest proxy = jdkTestDemo.getProxy();
        proxy.doOtherNotImportantThing("逍遥");
        proxy.doSomething("灵儿");

        List<String> list = new ArrayList<>();
        String s = "你好吗";
        list.add(s);
        list.add("我还好");
        list.add("去你的");
        list.add("我不好");
        list.add("what");
        list.add("你怎么好");
        Iterator<String> lt_b = list.iterator();
        System.out.println(list);
        List list2 = new ArrayList();
        list2.addAll(list);
//        while(lt_b.hasNext()){
//            String next = lt_b.next();
//            if (next.contains("好")){
//                lt_b.remove();
//            }
//        }
        for (String ls:list) {
            if (ls.contains("好")){
                list2.remove(ls);
            }
        }
        System.out.println(list2);
        System.out.println(list);
    }
}
