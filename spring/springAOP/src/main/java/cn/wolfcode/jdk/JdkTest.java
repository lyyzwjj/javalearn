package cn.wolfcode.jdk;

/**
 * @ClassName JdkTest
 * @Description 被代理的主体需要实现的接口
 * @Author xdzhango
 * @Date 2020/5/19 14:55
 **/
public interface JdkTest {
    String doSomething(String thingsNeedParm);

    String doOtherNotImportantThing(String otherThingsNeedParm);
}
