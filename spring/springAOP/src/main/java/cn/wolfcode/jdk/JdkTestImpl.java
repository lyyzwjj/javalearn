package cn.wolfcode.jdk;

/**
 * @ClassName JdkTestImpl
 * @Description JDK动态代理接口实现类
 * @Author xdzhango
 * @Date 2020/5/19 14:56
 **/
public class JdkTestImpl implements JdkTest{

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String doSomething(String thingsNeedParm) {
        System.out.println("使用" + thingsNeedParm + "做了一些事情");
        return "调用成功";
    }

    @Override
    public String doOtherNotImportantThing(String otherThingsNeedParm) {
        System.out.println("使用" + otherThingsNeedParm + "做了二些事情");
        return "调用成功";
    }
}
